var db = require('./database');
module.exports  = app => {
    
    app.get("/v1/user", (req, resp) => {
        db.find({}).exec((err, doc) => {
            if (err) return console.log(err);
            resp.json(doc);
        });
    });

    app.post("/v1/user", (req, resp) => {
        req.body.forEach((user) => {
            console.log(user._id)
            if(user._id){
                db.update({ _id : user._id }, user, {}, (err, numReplaced) => {
                    if(err) console.log('Erro ao editar: ');
                    else console.log('Editado com sucesso: ' + numReplaced);
                });  

            }else{
                db.insert(user, (err, newDoc) => {
                    if(err) console.log('Erro ao adicionar');
                    else console.log('Adicionado com sucesso: ' + newDoc);
                });  
            }
        });
        resp.status(200).end();
    });

    app.delete("/v1/user", (req, resp) => {
        db.remove({}, { multi: true },(err, numRemoved) => {
            if (err) return console.log(err);
            console.log('removido com sucesso');
            if(numRemoved) resp.status(200).end();
            resp.status(500).end();
        });
    });
};